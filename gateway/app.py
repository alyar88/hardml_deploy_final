from flask import Flask, jsonify, request
import requests

app = Flask(__name__)
EMB_SERVING_URL = 'http://localhost:8501/v1/models/model:predict'


@app.post('/')
def get_answer():
    data = request.json
    question = data['question']
    emb_serving_resp = requests.post(EMB_SERVING_URL, json={"inputs": [question]}).json()
    emb = emb_serving_resp['outputs'][0]
    return emb


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5500)
